﻿# Doctrin-Sample-API

## Running the application:

1. Remove the existing Docker image (if exists):

    `docker rmi doctrin/sample-api -f`

2. Build the Docker image:

    `cd` into the project root folder and execute the following command:
    `docker build -t doctrin/sample-api .`

3. Run the Docker container:

    To spin a Docker container from the previously built image execute the following command:

```bash
docker run -it \
-p 1337:1337 \
-e NODE_ENV=development \
-e PORT=1337 \
-e DOCKER_IP="$(docker-machine ip $(docker-machine active))" \
-e DB_HOST=doctrin-sample-db.chesipkqcyji.eu-central-1.rds.amazonaws.com \
-e DB_USERNAME=root \
-e DB_PASSWORD=6Z12Ym9KukRt4xJZ \
-e DB_NAME=doctrin_sample_db \
-e DB_PORT=5432 \
-v "$PWD/app:/usr/src/app" \
doctrin/sample-api
```

The application can then be accessed on the IP and PORT printed in the console.

## Database:

### Migrate to the latest data model:

`knex migrate:rollback --knexfile ./app/database/knexfile.js`

`knex migrate:latest --knexfile ./app/database/knexfile.js`

### INSERT seed data:

`knex seed:run --knexfile ./app/database/knexfile.js`
