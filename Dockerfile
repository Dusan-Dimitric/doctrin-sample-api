﻿# Use Node.js 7.10.
from node:alpine

# Create app directory.
RUN mkdir -p /usr/src/app

# Bundle the application files.
COPY package.json /usr/src/
COPY .eslintrc /usr/src/
COPY ./app /usr/src/app

# Install the application's dependencies.
WORKDIR /usr/src

RUN npm install -g supervisor
RUN npm install

RUN pwd
RUN ls -alh

# Change the ownership of the application folder to the `node` user.
RUN chown -R node:node /usr/src
USER node

# The exposed port.
EXPOSE 1337

# In order for supervisor to watch file changes, we need to set up a docker volume.
VOLUME ["/usr/src/app"]

# Finally - run the application.
CMD ["npm", "start"]
