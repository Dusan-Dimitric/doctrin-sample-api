/**
 * server.js - Application execution starts here.
 *
 * This file is responsible for starting the server as well as setting up the entire application.
 */

'use strict'

const express    = require('express')
const bodyParser = require('body-parser')

const auth                      = require('./middleware/auth')
const HATEOAS                   = require('./utils/HATEOAS/generate-links')
const return404                 = require('./middleware/standard_responses/404')
const printServerAddressAndPort = require('./utils/server/print-server-address-and-port')

/* Initialize the express application. */
let app = express()

/* Perform the necessary setup. */
app.set('port', process.env.PORT || 1337)

// Use the `body-parser` module in order to receive JSON payloads.
app.use(bodyParser.json())

/* Set up routes. */

let manageClinicsRoutes = require('./routers/manage/clinics')

app.use('/manage/clinics', auth.protect(), manageClinicsRoutes)

app.use('/', (req, res) => res.status(200).json(HATEOAS.generate.rootLevelLinks()))

/* Catch all the requests for URIs that don't exist. */
app.use(return404)

/* Start the server. */
app.listen(app.get('port'), printServerAddressAndPort(app.get('port')))
