/**
 * print-server-address-and-port.js - exposes a single function which just prints out the server's 
 *                                    IP address and port.
 */

'use strict'

const dns = require('dns')
const os  = require('os')

module.exports = PORT => {
    return () => {
        if (process.env.DOCKER_IP) {
            // The app is being run in a Docker container.
            console.log(`Listening at: ${process.env.DOCKER_IP}:${PORT}`)
        } else {
            // The app is being run directly.
            dns.lookup(os.hostname(), (err, add) => console.log(`Listening at: ${add}:${PORT}`))
        }
    }
}