/**
 * generate-links.js - generates different HATEOAS links for different scenarios.
 */

'use strict'

module.exports = {

    generate: {

        manage: {
        
            clinicLinks(data) {

                let clinics

                if (data.results) {
                    clinics = data.results
                } else {
                    clinics = data
                }

                clinics.map(clinic => Object.assign(clinic, 
                    {
                        "__links": [
                            {
                                "rel"    : "clinics",
                                "action" : "Display full data about this clinic.",
                                "method" : "GET",
                                "href"   : "/manage/clinics/" + clinic.clinic_id
                            },
                            {
                                "rel"    : "clinics",
                                "action" : "Update multiple fields of this clinic.",
                                "method" : "PUT",
                                "href"   : "/manage/clinics/" + clinic.clinic_id
                            },
                            {
                                "rel"    : "clinics",
                                "action" : "Update a single of this clinic.",
                                "method" : "PATCH",
                                "href"   : "/manage/clinics/" + clinic.clinic_id
                            },
                            {
                                "rel"    : "clinics",
                                "action" : "Delete this clinic.",
                                "method" : "DELETE",
                                "href"   : "/manage/clinics/" + clinic.clinic_id
                            }
                        ]
                    })
                )


                if (data.results) {
                    data.results = clinics
                    clinics = data
                }

                return clinics

            }
        
        },

        rootLevelLinks() {
            return {
                
                "__links": {
                    
                    "manage": {
                        "clinics": [
                            {
                                "rel"    : "clinics",
                                "action" : "Get a list of clinics.",
                                "method" : "GET",
                                "href"   : "/manage/clinics"
                            },
                            {
                                "rel"    : "clinics",
                                "action" : "Create a clinic.",
                                "method" : "POST",
                                "href"   : "/manage/clinics"
                            }
                        ]                          
                    },
                
                    "public": {

                    }

                }
            
            }
        }        

    }

}