/**
 * wrap-response-in-envelope.js - Wraps an array of results into a pre-defined, formatted data 
 *                                structure suitable for a HTTP response.
 */

'use strict'

module.exports = (results, resultsTotal, errors) => {
    
    if (errors) {
        return {
            success : false,
            errors  : errors
        }
    } else {
        return {
            success       : true,
            results       : results,
            results_total : parseInt(resultsTotal, 10)
        }
    }   
    
}