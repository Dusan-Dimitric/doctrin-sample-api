/**
 * clinics.js - Defines all the clinics endpoints.
 */

'use strict'

const express = require('express')
let router    = express.Router()

const Clinics = require('../../controllers/clinic-controller')

router.get(   '/',          Clinics.manage.read.multiple)
router.post(  '/',          Clinics.manage.create.single)

router.get(   '/:clinicID', Clinics.manage.read.single)
router.put(   '/:clinicID', Clinics.manage.update.single)
router.patch( '/:clinicID', Clinics.manage.update.single)
router.delete('/:clinicID', Clinics.manage.delete.single)

module.exports = router
