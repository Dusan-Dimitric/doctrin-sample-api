/**
 * 404.js - A standard, catch-all 404 response when the requested URI isn't registered by the 
 *			application. 
 */

'use strict'

let wrapResponse = require('../../utils/wrap-response-in-envelope')

module.exports = (req, res) => {
    res.status(404).json(wrapResponse(null, null, [
        {
            "message": "404 - Not Found"
        }
    ]))
}