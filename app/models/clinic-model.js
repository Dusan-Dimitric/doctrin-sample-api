/**
 * clinic-model.js
 */

'use strict'

const KNEX = require('../database/db')

let ClinicModel = module.exports = {

    public: {

        columns: ['*'],

        read: {

            multiple() {

            }

        }

    },

    manage: {

        columns: ['*'],

        create: {

            single(clinicCreateObject) {
                return KNEX.insert(clinicCreateObject)
                           .returning('clinic_id')
                           .into('clinics')
                           .then(insertedClinicID => {
                               
                               if (insertedClinicID.length <= 0) {

                                   // TODO: Throw a 500 or 409 or whatever is suitable.

                               } else {
                                   return ClinicModel.manage.read.single(insertedClinicID[0])
                               }
                        
                           })
            }

        },
        
        read: {

            single(clinicID) {
                return KNEX.select('*')
                           .from('full_clinic_view')
                           .where('clinic_id', clinicID)
            },

            multiple() {
                
                let finalResult = {
                    results       : null,
                    results_total : null
                }
                
                return KNEX.select(ClinicModel.public.columns).from('clinics')
                    .then(clinics => {

                        finalResult.results = clinics

                        // Count all the clinics.
                        return KNEX.select(KNEX.raw('count(*)')).from('clinics')
                    })
                    .then(clinicsCount => {

                        finalResult.results_total = clinicsCount[0].count

                        return finalResult
                    })
            }

        },

        update: {

            single(clinicID, clinicUpdateObject) {
                return ClinicModel.manage.read.single(clinicID)
                    .then(clinics => {
                        if (clinics.length <= 0) {
                            // TODO: Throw a 404.
                        } else {

                            return KNEX('clinics').update(clinicUpdateObject)
                                                  .where('clinic_id', clinics[0].clinic_id)
                                                  .limit(1)
                        }
                    })
                    .then(updateResults => {

                        if (updateResults !== 1) {
                            // TODO: Throw a 409, 500 or other appropriate errors.
                        } else {
                            return ClinicModel.manage.read.single(clinicID)
                        }
                               
                    })
            }

        },

        delete: {

            single(clinicID) {
                return ClinicModel.manage.read.single(clinicID)
                    .then(clinics => {
                        if (clinics.length <= 0) {
                            // TODO: Throw a 404.
                        } else {
                            return KNEX.delete().from('clinics').where('clinic_id', clinics[0].clinic_id)
                        }
                    })
                    .then(deleteResults => {
                        
                        if (deleteResults !== 1) {
                            // TODO: Throw a 409 or other appropriate error messages.
                        } else {
                            return deleteResults
                        }
                    })
            }

        }

    }

}
