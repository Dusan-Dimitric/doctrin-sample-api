/**
 * 20170514165627_doctrin_sample_db.js - `doctrin_sample_db` table definitions.
 */

'use strict'

exports.up = knex => {

    return knex.schema.createTableIfNotExists('people', table => {

        // Columns:
        table.bigIncrements('person_id')
        table.date         ('date_of_birth').notNullable()

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('names', table => {

            // Columns:
            table.bigIncrements('id')
            table.integer      ('person_id')       .notNullable()
            table.string       ('title'     ,  100)
            table.string       ('first_name',  255).notNullable()
            table.string       ('middle_name', 255)
            table.string       ('last_name',   255).notNullable()
            table.timestamp    ('created_at')      .notNullable().default(knex.fn.now())

            // Table constraints:
            table.foreign('person_id').references('people.person_id')

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('clinics', table => {

            // Columns:
            table.bigIncrements('clinic_id')
            table.string       ('clinic_name', 255)                  .unique().notNullable()
            table.integer      ('parent_clinic_id')
            table.string       ('clinic_type', 255)
            table.specificType ('emergency_phones', 'varchar(255)[]')
            table.string       ('email', 255)

            // Table constraints:
            table.foreign('parent_clinic_id').references('clinics.clinic_id')

        })

    })

    .then(() => {
        return knex.schema.createTableIfNotExists('medical_staff', table => {

            // Columns:
            table.bigIncrements('staff_id')
            table.integer      ('person_id')
            table.integer      ('clinic_id')                
            table.string       ('position', 255)
            table.time         ('shift_start')
            table.time         ('shift_end')
            table.date         ('contract_start').notNullable()
            table.date         ('contract_end')                .default(null)

            // Table constraints:
            table.foreign('person_id').references('people.person_id')
            table.foreign('clinic_id').references('clinics.clinic_id')

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('patients', table => {

            // Columns:
            table.bigIncrements('patient_id')
            table.integer      ('person_id')                     .notNullable()
            table.specificType ('allergies',    'varchar(255)[]')
            table.text         ('precautions')
            table.specificType ('cardio',       'varchar(255)[]')
            table.specificType ('respiratory',  'varchar(255)[]')
            table.enu          ('diet',         [ 'paleo', 'frutarian', 'vegan', 'chrono' ])
            table.enu          ('blood_type',   [ 'O+', 'O-', 'A+', 'A-', 'B+', 'B-', 'AB+', 'AB-' ])

            // Table constraints:
            table.foreign('person_id').references('people.person_id')

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('patient_visits', table => {

            // Columns:
            table.bigIncrements('visit_id')
            table.integer      ('patient_id')   .notNullable()
            table.integer      ('clinic_id')    .notNullable()
            table.timestamp    ('scheduled_at')
            table.boolean      ('visited')      .notNullable().default(false)

            // Table constraints:
            table.foreign('patient_id').references('patients.patient_id')
            table.foreign('clinic_id') .references('clinics.clinic_id')

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('issues', table => {

            // Columns:
            table.bigIncrements('issue_id')
            table.integer      ('visit_id')         .unique().notNullable()
            table.text         ('issue_description')         .notNullable()
            table.decimal      ('price', 10, 2)
            table.boolean      ('resolved')                                .default(false)

            // Table constraints:
            table.foreign('visit_id').references('patient_visits.visit_id')

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('addresses', table => {

            // Columns:
            table.bigIncrements('address_id')
            table.enu          ('address_type', ['Invoicing', 'Visiting'])
            table.specificType ('country', 'char(2)')   .notNullable()
            table.string       ('state',         255)
            table.string       ('city',          255)
            table.string       ('postal_code',   255)
            table.string       ('street',        255)
            table.string       ('street_number', 10)

            // Table constraints:

        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('patient_addresses', table => {

            // Columns:
            table.integer('patient_id')
            table.integer('address_id')

            // Table constraints:
            table.primary(['patient_id', 'address_id'])
        })

    })
    .then(() => {
        return knex.schema.createTableIfNotExists('clinic_addresses', table => {

            // Columns:
            table.integer('clinic_id')
            table.integer('address_id')

            // Table constraints:
            table.primary(['clinic_id', 'address_id'])
        })

    })
    .then(() => {
        return knex.raw(`CREATE OR REPLACE VIEW full_patient_view AS
                            SELECT
	                            pp.person_id,
	                            pp.date_of_birth,

	                            nm.title,
	                            nm.first_name,
	                            nm.middle_name,
	                            nm.last_name,
	                            nm.created_at,

	                            pt.patient_id,
	                            pt.allergies,
	                            pt.precautions,
	                            pt.cardio,
	                            pt.respiratory,
	                            pt.diet,
	                            pt.blood_type,

	                            /* Select patient's addresses as a JSON array. */
	                            (SELECT array_to_json(array_agg(addresses)) AS addresses FROM (
		                            SELECT
                                        ad.address_type,
			                            ad.country,
			                            ad.state,
			                            ad.city,
			                            ad.postal_code,
			                            ad.street,
			                            ad.street_number
		                            FROM patient_addresses AS pa
		                            INNER JOIN addresses AS ad ON ad.address_id = pa.address_id
	                            ) AS addresses),

	                            /* Select patient's visits as a JSON array. */
	                            (SELECT array_to_json(array_agg(patient_visits)) AS patient_visits FROM (
		                            SELECT
			                            pv.patient_id,
			                            pv.scheduled_at
		                            FROM patient_visits AS pv
		                            INNER JOIN patients AS pa ON pv.patient_id = pa.patient_id
	                            ) AS patient_visits)

                            FROM
	                            patients AS pt

                            LEFT JOIN people AS pp ON pp.person_id = pt.person_id

                            /* Left join with the name that was submitted the latest. */
                            LEFT JOIN (SELECT DISTINCT ON (person_id) * FROM names ORDER BY person_id, created_at DESC) AS nm ON nm.person_id = pt.person_id;`)
    })
    .then(() => {
        return knex.raw(`CREATE OR REPLACE VIEW full_staff_view AS
                            SELECT                                
                                pp.person_id,
                                pp.date_of_birth,

                                ms.staff_id,
                                ms.clinic_id,
                                ms.position,
                                ms.shift_start,
                                ms.shift_end,

                                nm.title,
                                nm.first_name,
                                nm.middle_name,
                                nm.last_name,
                                nm.created_at
                            FROM
                                medical_staff AS ms
                            
                            LEFT JOIN people AS pp ON pp.person_id = ms.person_id

                            /* Left join with the name that was submitted the latest. */
                            LEFT JOIN (SELECT DISTINCT ON (person_id) * FROM names ORDER BY person_id, created_at DESC) AS nm ON nm.person_id = ms.person_id;`)
    })
    .then(() => {
        return knex.raw(`CREATE OR REPLACE VIEW full_clinic_view AS
                            SELECT 
                                cl.clinic_id AS clinic_id,
                                cl.clinic_name,
    
                                (SELECT array_to_json(array_agg(addresses)) AS addresses FROM (
                                    SELECT
                                        ad.address_type,
                                        ad.country,
                                        ad.state,
                                        ad.city,
                                        ad.postal_code,
                                        ad.street,
                                        ad.street_number
                                    FROM clinic_addresses AS ca
                                    INNER JOIN addresses AS ad ON ad.address_id = ca.address_id
                                ) AS addresses),
    
                                (SELECT array_to_json(array_agg(patient_visits)) AS patient_visits FROM (
                                    SELECT
                                        pv.patient_id,
                                        pv.scheduled_at
                                    FROM patient_visits AS pv
                                    INNER JOIN clinics  AS cl ON pv.clinic_id = cl.clinic_id
                                ) AS patient_visits)

                            FROM 
                                clinics AS cl;`)
    })
    .catch(console.error)

}

exports.down = knex => {

    // Drop the views and tables.
    return knex.raw('DROP VIEW IF EXISTS full_staff_view, full_patient_view, full_clinic_view')
        .then(() => knex.schema.dropTableIfExists('names'))
        .then(() => knex.schema.dropTableIfExists('medical_staff'))
        .then(() => knex.schema.dropTableIfExists('issues'))
        .then(() => knex.schema.dropTableIfExists('patient_visits'))
        .then(() => knex.schema.dropTableIfExists('patients'))
        .then(() => knex.schema.dropTableIfExists('people'))
        .then(() => knex.schema.dropTableIfExists('clinics'))
        .then(() => knex.schema.dropTableIfExists('addresses'))
        .then(() => knex.schema.dropTableIfExists('patient_addresses'))
        .then(() => knex.schema.dropTableIfExists('clinic_addresses'))
        .catch(console.error)

}
