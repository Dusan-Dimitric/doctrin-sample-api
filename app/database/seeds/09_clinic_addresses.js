/**
 * 09_clinic_addresses.js - `clinic_addresses` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'clinic_addresses'

    const ROWS = [
        {
            "address_id" : 2,
            "clinic_id"  : 1
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
