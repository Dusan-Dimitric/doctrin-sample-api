/**
 * 02_patients.js - `patients` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'patients'

    const ROWS = [
        {
            "person_id"     : 1,
            "allergies"     : ["dust", "cigatrettes"],
            "precautions"   : null,
            "cardio"        : [],
            "respiratory"   : [],
            "diet"          : "paleo",
            "blood_type"    : "A+"
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
