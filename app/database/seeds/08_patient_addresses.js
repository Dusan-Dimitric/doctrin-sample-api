/**
 * 08_patient_addresses.js - `patient_addresses` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'patient_addresses'

    const ROWS = [
        {
            "address_id" : 1,
            "patient_id" : 1
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
