/**
 * 04_medical_staff.js - `medical_staff` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'medical_staff'

    const ROWS = [
        {
            "person_id"      : 2,
            "clinic_id"      : 1,
            "position"       : "Intern",
            "shift_start"    : "18:00:00.000Z",
            "shift_end"      : "02:00:00.000Z",
            "contract_start" : "2017-01-06",
            "contract_end"   : null
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
