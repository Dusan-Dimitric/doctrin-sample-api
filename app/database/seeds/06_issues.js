/**
 * 06_issues.js - `issues` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'issues'

    const ROWS = [
        {
            "visit_id"          : 1,
            "issue_description" : "Identified back pain..",
            "price"             : 100.00,
            "resolved"          : false
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
