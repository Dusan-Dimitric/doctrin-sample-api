/**
 * 07_addresses.js - `addresses` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'addresses'

    const ROWS = [
        {
            "country"       : "AT",
            "state"         : null,
            "city"          : null,
            "postal_code"   : null,
            "street"        : null,
            "street_number" : null
        },
        {
            "country"       : "SE",
            "state"         : null,
            "city"          : null,
            "postal_code"   : null,
            "street"        : null,
            "street_number" : null
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
