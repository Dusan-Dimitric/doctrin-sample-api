/**
 * 00_people.js - `people` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'people'

    const ROWS = [
        {
            "date_of_birth": "1986-05-14"
        },
        {
            "date_of_birth": "1984-04-13"
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
