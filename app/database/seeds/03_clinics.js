/**
 * 03_clinics.js - `clinics` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'clinics'

    const ROWS = [
        {
            "clinic_name"      : "NeoClinic",
            "parent_clinic_id" : null,
            "clinic_type"      : null,
            "emergency_phones" : ["(+333) 333-3333"],
            "email"            : "neoclinic@imaginary.domain"
        },
        {
            "clinic_name"      : "TechClinic",
            "parent_clinic_id" : null,
            "clinic_type"      : null,
            "emergency_phones" : ["(+444) 444-444"],
            "email"            : "techclinic@imaginary.domain"
        },
        {
            "clinic_name"      : "EcoClinic",
            "parent_clinic_id" : null,
            "clinic_type"      : null,
            "emergency_phones" : ["(+555) 555-5555"],
            "email"            : "ecoclinic@imaginary.domain"
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
