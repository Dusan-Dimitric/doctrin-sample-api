/**
 * 01_names.js - `names` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'names'

    const ROWS = [
        {
            "person_id": 1,
            "first_name" : "John",
            "last_name"  : "Doe"
        },
        {
            "person_id": 1,
            "first_name" : "Jane",
            "last_name"  : "Doe"
        },
        {
            "person_id": 2,
            "first_name" : "Josh",
            "last_name"  : "Priestly"
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
