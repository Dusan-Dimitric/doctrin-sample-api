/**
 * 05_patient_visits.js - `patient_visits` table seed data.
 */

'use strict'

exports.seed = knex => {

    const tableName = 'patient_visits'

    const ROWS = [
        {
            "patient_id"     : 1,
            "clinic_id"      : 1,
            "scheduled_at"   : "2017-05-15T19:00:00.000Z",
            "visited"        : false
        }
    ]
    
    // Deletes ALL existing entries
    return knex(tableName).del()
        .then(() => {
            // Inserts seed entries
            return knex(tableName).insert(ROWS)
        })

}
