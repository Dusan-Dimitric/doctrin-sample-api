/**
 * db.js - a module for all communications with the database.
 */

'use strict'

/*
 * The next 2 lines of code make sure that the `DECIMAL` PostgreSQL data type gets returned as a floating point number,
 * not as a string (which is the default case). 1700 - is the `oid` code of the PostgreSQL's `DECIMAL` number type.
 *
 * For more details about this behaviour - refer to this GitHub issue: "https://github.com/tgriesser/knex/issues/927"
 */
let pg = require('pg')
pg.types.setTypeParser(1700, 'text', parseFloat)

const KNEX_CONFIG = require('./knexfile.js')
const KNEX        = require('knex')(KNEX_CONFIG[process.env.NODE_ENV || 'local'])

module.exports = KNEX
