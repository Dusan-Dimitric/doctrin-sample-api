/**
 *  knexfile.js - 'knex' ORM's configuration file.
 */

'use strict'

// Just used for shortening the access to the environment variables.
const ENV = process.env

/*
 * `module.exports` exposes the PostgreSQL configuration object.
 * The exports object can be modified in order to use other database systems supported by 'knex' 
 * (MySQL, MariaDB ...), if the need arises.
 */
module.exports = {

    local: { // For local database instances, when running the application directly, without Docker.
        client: 'pg',
        connection: {
            host     : 'localhost',
            user     : 'postgres',
            password : 'postgres',
            database : 'doctrin_sample_db',
            port     : 5432
        },
        debug: true
    },

    test: { // Test environment database config.
        client: 'pg',
        connection: {
            host     : 'doctrin-sample-db.chesipkqcyji.eu-central-1.rds.amazonaws.com',
            user     : 'root',
            password : '6Z12Ym9KukRt4xJZ',
            database : 'doctrin_sample_db',
            port     : 5432
        },
        debug: false
    },

    development: { // Development database config.
        client: 'pg',
        connection: {
            host     : ENV.DB_HOST,
            user     : ENV.DB_USERNAME,
            password : ENV.DB_PASSWORD,
            database : ENV.DB_NAME,
            port     : ENV.DB_PORT
        },
        debug: true
    },

    staging: { // Staging database config.
        client: 'pg',
        connection: {
            host     : ENV.DB_HOST,
            user     : ENV.DB_USERNAME,
            password : ENV.DB_PASSWORD,
            database : ENV.DB_NAME,
            port     : ENV.DB_PORT
        },
        debug: false
    },

    production: { // Production database config.
        client: 'pg',
        connection: {
            host     : ENV.DB_HOST,
            user     : ENV.DB_USERNAME,
            password : ENV.DB_PASSWORD,
            database : ENV.DB_NAME,
            port     : ENV.DB_PORT
        },
        debug: false
    }
    
}
