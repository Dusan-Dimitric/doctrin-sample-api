/**
 * clinic-controller.js
 */

'use strict'

const ClinicsModel = require('../models/clinic-model')
const HATEOAS      = require('../utils/HATEOAS/generate-links')
const wrapResponse = require('../utils/wrap-response-in-envelope')

module.exports = {

    public: {

        read: {

        }

    },

    manage: {

        create: {

            single(req, res) {

                let clinicCreateObject = req.body

                return ClinicsModel.manage.create.single(clinicCreateObject)
                    .then(HATEOAS.generate.manage.clinicLinks)
                    .then(data => res.status(201).json(wrapResponse(data, 1)))
                    .catch(console.error)
            }

        },

        read: {

            single(req, res) {

                const clinicID = parseInt(req.params.clinicID, 10)

                return ClinicsModel.manage.read.single(clinicID)
                    .then(HATEOAS.generate.manage.clinicLinks)
                    .then(data => res.status(200).json(wrapResponse(data, 1)))
                    .catch(console.error)
            },

            multiple(req, res) {
                return ClinicsModel.manage.read.multiple()
                    .then(HATEOAS.generate.manage.clinicLinks)
                    .then(data => res.status(206).json(wrapResponse(data.results, data.results_total)))
                    .catch(console.error)
            }

        },

        update: {

            single(req, res) {

                const clinicID = parseInt(req.params.clinicID, 10)

                let clinicUpdateObject = req.body

                return ClinicsModel.manage.update.single(clinicID, clinicUpdateObject)
                    .then(HATEOAS.generate.manage.clinicLinks)
                    .then(data => res.status(200).json(wrapResponse(data, 1)))
                    .catch(console.error)
            }

        },

        delete: {

            single(req, res) {

                const clinicID = parseInt(req.params.clinicID, 10)

                return ClinicsModel.manage.delete.single(clinicID)
                    .then(data => res.status(204).json(wrapResponse(data)))
                    .catch(console.error)
            }

        }

    }

}
