/**
 * api-endpoints-spec.js - Tests all the API routes.
 */

'use strict'

const superagent = require('superagent')
const chai       = require('chai')
const expect     = chai.expect

// TODO: Generate this hard-coded value on the fly.
const BASE_URL = `192.168.99.100:1337`

context('Doctrin - Sample API', function() {

    this.timeout(5000) // Set the timeout to 5 seconds for all endpoint requests.

    context('PRIVATE API:', () => {

        context('Clinic CRUD tests.', () => {

            let clinicColumns = [
                'clinic_id',
                'clinic_name',
                'parent_clinic_id',
                'clinic_type',
                'emergency_phones',
                'email',
                'addresses',
                'patient_visits',
                '__links'
            ]

            let newlyCreatedClinicID

            describe('CREATE clinic records.', () => {

                it('Creates a single clinic record.', done => {
                    
                    const clinicCreateObject = {
                        "clinic_name"       : "TestClinic",
                        "parent_clinic_id"  : null,
                        "clinic_type"       : null,
                        "emergency_phones"  : [
                            "(+333) 333-3333"
                        ],
                        "email"             : "testclinic@imaginary.domain"
                    }

                    superagent.post(`${BASE_URL}/manage/clinics`)
                        .send(clinicCreateObject)
                        .end((err, res) => {

                            expect(err).to.be.null
                            expect(res.statusCode).to.equal(201)
                            
                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(Array.isArray(body.results)).to.be.true
                            
                            body.results.forEach(result => {
                                expect(result).to.have.all.keys(clinicColumns)
                                expect(result.clinic_name).to.be.a('string')
                                expect(result.emergency_phones).to.be.an.instanceof(Array)
                                expect(result.email).to.be.a('string')
                            })

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.equal(1)

                            newlyCreatedClinicID = body.results[0].clinic_id

                            done()
                        })

                })

            })

            describe('READ clinic records.', () => {

                it('Reads a single clinic record.', done => {
                    
                    superagent.get(`${BASE_URL}/manage/clinics/${newlyCreatedClinicID}`)
                        .end((err, res) => {

                            expect(err).to.be.null
                            expect(res.statusCode).to.equal(200)
                            
                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(Array.isArray(body.results)).to.be.true
                            
                            body.results.forEach(result => {
                                expect(result).to.have.all.keys(clinicColumns)
                                expect(result.clinic_name).to.be.a('string')
                                expect(result.emergency_phones).to.be.an.instanceof(Array)
                                expect(result.email).to.be.a('string')
                                expect(result.addresses).to.be.an.instanceof(Array)
                                expect(result.patient_visits).to.be.an.instanceof(Array)
                            })

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.equal(1)

                            done()
                        })

                })

                it('Reads multiple clinic records.', done => {
                    
                    superagent.get(`${BASE_URL}/manage/clinics`)
                        .end((err, res) => {

                            expect(err).to.be.null
                            expect(res.statusCode).to.equal(206)
                            
                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(Array.isArray(body.results)).to.be.true
                            
                            body.results.forEach(result => {
                                expect(result).to.have.all.keys(clinicColumns)
                                expect(result.clinic_name).to.be.a('string')
                                expect(result.emergency_phones).to.be.an.instanceof(Array)
                                expect(result.email).to.be.a('string')
                            })

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.be.above(0)

                            done()
                        })

                })
                
            })

            describe('UPDATE clinic records.', () => {

                it('Updates multiple fields of a single clinic record.', done => {
                    
                    const clinicUpdateObj = {
                        "clinic_name"       : "TestClinicX", // <-- CHANGED.
                        "parent_clinic_id"  : null,
                        "clinic_type"       : "Cardio-Vascular",  // <-- CHANGED.
                        "emergency_phones"  : [
                            "(+333) 333-3333"
                        ],
                        "email"             : "testclinicx@imaginary.domain"  // <-- CHANGED.
                    }

                    superagent.put(`${BASE_URL}/manage/clinics/${newlyCreatedClinicID}`)
                        .send(clinicUpdateObj)
                        .end((err, res) => {

                            expect(err).to.be.null
                            expect(res.statusCode).to.equal(200)
                            
                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(Array.isArray(body.results)).to.be.true
                            
                            body.results.forEach(result => {
                                expect(result).to.have.all.keys(clinicColumns)
                                expect(result.clinic_name).to.be.a('string')
                                expect(result.emergency_phones).to.be.an.instanceof(Array)
                                expect(result.email).to.be.a('string')
                                expect(result.addresses).to.be.an.instanceof(Array)
                                expect(result.patient_visits).to.be.an.instanceof(Array)
                            })

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.equal(1)

                            // Check if the properties are updated.
                            expect(body.results[0].clinic_name).to.equal('TestClinicX')
                            expect(body.results[0].clinic_type).to.equal('Cardio-Vascular')
                            expect(body.results[0].email).to.equal('testclinicx@imaginary.domain')

                            done()
                        })
                })
                
                it('Updates a single field of a single clinic record.', done => {
                    
                    const clinicPatchObj = {
                        "clinic_name" : "TestClinicY" // <-- CHANGED.
                    }

                    superagent.patch(`${BASE_URL}/manage/clinics/${newlyCreatedClinicID}`)
                        .send(clinicPatchObj)
                        .end((err, res) => {

                            expect(err).to.be.null
                            expect(res.statusCode).to.equal(200)
                            
                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(Array.isArray(body.results)).to.be.true
                            
                            body.results.forEach(result => {
                                expect(result).to.have.all.keys(clinicColumns)
                                expect(result.clinic_name).to.be.a('string')
                                expect(result.emergency_phones).to.be.an.instanceof(Array)
                                expect(result.email).to.be.a('string')
                                expect(result.addresses).to.be.an.instanceof(Array)
                                expect(result.patient_visits).to.be.an.instanceof(Array)
                            })

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.equal(1)

                            // Check if the property is updated.
                            expect(body.results[0].clinic_name).to.equal('TestClinicY')

                            done()
                        })
                })

            })

            describe('DELETE clinic records.', () => {

                it('Deletes a single clinic record.', done => {
                    superagent.delete(`${BASE_URL}/manage/clinics/${newlyCreatedClinicID}`)
                        .end((err, res) => {

                            expect(err).to.equal(null)

                            expect(res.statusCode).to.equal(204)

                            done()

                        })
                })

            })

        })

    })

    context('PUBLIC API:', () => {

        context.skip('SCENARIO 1: Visiting a clinic.', () => {

            let visitColumns = [
                'patient_id',
                'clinic_id',
                'scheduled_at'
            ]

            describe('POST /clinics/{clinic_id}/visits', () => {

                it.skip('Schedules a visit to a clinic.', done => {

                    superagent.post(`${BASE_URL}/clinics/{clinic_id}/visits`)
                        .end((err, res) => {

                            expect(err).to.be.null

                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(body.results[0]).to.have.all.keys(visitColumns)

                            expect(Array.isArray(body.results)).to.be.true
                            expect(body.results[0].scheduled_at).to.be.a('string')

                            expect(Array.isArray(body.results.issues)).to.be.true

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.be.above(0)

                            done()

                        })

                })

            })

        })

        
        context.skip('SCENARIO 2: Changing the address.', () => {

            let addressColumns = [
                // TODO: Add address columns.
            ]

            describe('PUT /me', () => {

                it.skip('Changes/updates the addresses.', done => {

                    superagent.post(`${BASE_URL}/me`)
                        .end((err, res) => {

                            expect(err).to.be.null

                            let body = res.body

                            expect(body).to.be.an('object')

                            expect(body.success).to.be.a('boolean')
                            expect(body.success).to.be.true

                            expect(body.results[0]).to.have.all.keys(addressColumns)

                            expect(Array.isArray(body.results)).to.be.true

                            expect(Number.isInteger(body.results_total)).to.be.true
                            expect(body.results_total).to.be.above(0)

                            done()

                        })

                })

            })

        })

    })
    
})
